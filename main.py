import json
import datetime
import math
import itertools
import matplotlib.pyplot as plt


def listeners(data):
    genre = {}
    for cle, _ in data.items():
        for x in data[cle]["music"]:
            if x not in genre.keys():
                genre[x] = 1
            else:
                genre[x] += 1
    return genre


def average_age(data):
    music_age = {}
    for cle, _ in data.items():
        x = datetime.datetime.now()
        birthdate = data[cle]["birthdate"].split("-")
        y = datetime.datetime(int(birthdate[0]), int(birthdate[1]), int(birthdate[2]))
        age_jour = str(x - y).split(" ")[0]
        age = math.floor(int(age_jour) / 365)
        for x in data[cle]["music"]:
            if x not in music_age.keys():
                music_age[x] = []
                music_age[x].append(age)
            else:
                music_age[x].append(age)
    for cle, _ in music_age.items():
        music_age[cle] = (1 / len(music_age[cle])) * sum(music_age[cle])
    return music_age


def age_pyramid(data, city, slice_size):
    data_city = {}
    for cle, _ in data.items():
        if city in data[cle].values():
            data_city[cle] = data[cle]
    liste_age = []
    for cle, _ in data_city.items():
        x = datetime.datetime.now()
        birthdate = data_city[cle]["birthdate"].split("-")
        y = datetime.datetime(int(birthdate[0]), int(birthdate[1]), int(birthdate[2]))
        age_jour = str(x - y).split(" ")[0]
        age = math.floor(int(age_jour) / 365)
        liste_age.append(age)
    # liste_age_class = [(cle, list(group)) for (cle, group) in itertools.groupby(sorted(liste_age), key=lambda x: x // slice_size)]
    plt.barh(liste_age, width=slice_size)
    plt.show()


if __name__ == "__main__":
    with open("data_rhobs.json/data_rhobs.json", "r") as file:
        data = json.load(file)

    a = listeners(data)
    print(a)

    b = average_age(data)
    print(b)

    # Does not work !
    age_pyramid(data, "Blin", 10)
