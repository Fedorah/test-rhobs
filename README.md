# Test RHOBS

Test for RHOBS internship

## Dependencies

In a terminal at root folder do:

```
pip install -r requirements.txt
```

The project needs a folder named `data_robs.json` with a file named `data_rhobs.json` inside this folder.

## Quickstart

To run the code do in a terminal:

```
python main.py
```

It will the 3 functions demanded for the test.

The third function does not fulfill the requirements.
